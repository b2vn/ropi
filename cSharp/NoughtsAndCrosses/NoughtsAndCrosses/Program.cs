﻿using System;

namespace NoughtsAndCrosses
{
	public enum EPiece
	{
		Blank,
		Nought,
		Cross
	};

	public interface IPiece
	{
		EPiece getValue ();
	};

	// TODO: IPiece could be implemented as a lazy initializer 
	// pattern with string and value as the only member variables
	public class Blank : IPiece
	{
		public override string ToString ()
		{
			return string.Format (" ");
		}

		public EPiece getValue ()
		{
			return EPiece.Blank;
		}
	};

	public class Nought : IPiece
	{
		public override string ToString ()
		{
			return string.Format ("O");
		}

		public EPiece getValue ()
		{
			return EPiece.Nought;
		}
	};

	public class Cross : IPiece
	{
		public override string ToString ()
		{
			return string.Format ("X");
		}

		public EPiece getValue ()
		{
			return EPiece.Cross;
		}
	};

	public class Board
	{
		// TODO: Keep track on whos turn it is to play.
		// TODO: Board size is hardcoded. Make it possible to play on other size 
		// boards, e.g. a 4x4 board.
		public IPiece[,] fields = new IPiece[3, 3];

		public Board ()
		{
			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
					fields [i, j] = new Blank ();
		}

		public override string ToString ()
		{
			string ret = "\n";

			for (int i = 0; i < 3; ++i) {
				// print row
				if (i != 0)
					ret += "═══╬═══╬═══\n";
				
				for (int j = 0; j < 3; ++j) {
					// print field
					if (j != 0)
						ret += "║";
					
					ret += " " + fields [i, j].ToString () + " ";
				}
				ret += "\n";
			}

			ret += "status: ";
			// TODO: Real status is missing. 
			ret += "???";
			ret += "\n";

			return ret;
		}

		public void placePiece (EPiece piece, int x, int y)
		{
			// TODO: Notify player if no piece was played
			// TODO: Make it possible to move an existing piece when the player
			// has three pieces on the board
			// NOTE: This is a good method to use as TDD example
			if (fields [x, y] is Blank) {
				fields [x, y] = lazyCreate (piece);
			}
		}

		// TODO: Is the enum really needed? Could this be done via 
		protected IPiece lazyCreate (EPiece piece)
		{
			if (piece == EPiece.Cross)
				return new Cross ();
			if (piece == EPiece.Nought)
				return new Nought ();
			return new Blank ();
		}

		public EPiece validate ()
		{
			// TODO: Validate diagonals
			for (int i = 0; i < 3; ++i) {
				if (fields [i, 0].getValue () == fields [i, 1].getValue () 
					&& fields [i, 0].getValue () == fields [i, 2].getValue ())
					return fields [i, 0].getValue ();
				if (fields [0, i].getValue ()
				    == fields [1, i].getValue () 
					&& fields [0, i].getValue () == fields [2, i].getValue ())
					return fields [0, i].getValue ();
			}

			return EPiece.Blank;
		}

		// TODO: Make a function that returns a "game score" that can be used by the
		// AI to validate if one move is better than another.
	};

	// TODO: Create generic class for a player
	// TODO: Create class for computer intelligence

	class MainClass
	{
		

		public static void Main (string[] args)
		{
			Board board = new Board ();

			// TODO: Allow user to input move
			// TODO: Make method that moves piece, validate the game and prints the board
			// TODO: Make sure no players can place any pieces when a winner is found
			Console.WriteLine (board);
			board.placePiece (EPiece.Cross, 0, 0);
			Console.WriteLine (board);
			board.placePiece (EPiece.Nought, 1, 1);
			Console.WriteLine (board);

			// Make a winner
			Console.Write ("Winner is: " + board.validate ());
			board.placePiece (EPiece.Nought, 0, 1);
			board.placePiece (EPiece.Nought, 2, 1);
			Console.WriteLine (board);
			Console.Write ("Winner is: " + board.validate ());
		}
	}
}
