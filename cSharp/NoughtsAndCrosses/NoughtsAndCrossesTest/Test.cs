﻿using NUnit.Framework;
using System;
using NoughtsAndCrosses;

namespace NoughtsAndCrossesTest
{
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void TestInitialState()
		{
			Board b = new Board ();

			Assert.IsTrue (b.fields [0, 0] is Blank);
			Assert.IsTrue (b.fields [0, 1] is Blank);
			Assert.IsTrue (b.fields [0, 2] is Blank);
			Assert.IsTrue (b.fields [1, 0] is Blank);
			Assert.IsTrue (b.fields [1, 1] is Blank);
			Assert.IsTrue (b.fields [1, 2] is Blank);
			Assert.IsTrue (b.fields [2, 0] is Blank);
			Assert.IsTrue (b.fields [2, 1] is Blank);
			Assert.IsTrue (b.fields [2, 2] is Blank);
		}

		[Test()]
		public void TestPlacePiece() {
			Board b = new Board ();

			b.placePiece (EPiece.Cross, 2, 1);
			Assert.IsTrue (b.fields [2, 1] is Cross);
			b.placePiece (EPiece.Cross, 1, 0);
			Assert.IsTrue (b.fields [1, 0] is Cross);
			b.placePiece (EPiece.Nought, 1, 1);
			Assert.IsTrue (b.fields [1, 1] is Nought);

			// ... and no other must have changed
			Assert.IsTrue (b.fields [0, 0] is Blank);
			Assert.IsTrue (b.fields [0, 1] is Blank);
			Assert.IsTrue (b.fields [0, 2] is Blank);
			Assert.IsTrue (b.fields [1, 2] is Blank);
			Assert.IsTrue (b.fields [2, 0] is Blank);
			Assert.IsTrue (b.fields [2, 2] is Blank);
		}

		[Test()]
		public void TestPlacePieceCollision() {
			// TODO: Test that the code can handle (and reject) if two pieces collide
		}
	}
}

