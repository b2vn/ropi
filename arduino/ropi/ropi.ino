#include <PinChangeInt.h>

#define MOTOR1_AKTIV    3// PWM udgang
#define MOTOR1_FREM_AD  2 
#define MOTOR1_TILBAGE  4 
#define MOTOR1_SIGNAL    // Digital indgang

#define MOTOR2_AKTIV    5// PWM udgang
#define MOTOR2_FREM_AD  7 
#define MOTOR2_TILBAGE  8 
#define MOTOR2_SIGNAL    // Digital indgang

#define LYS              // Digital udgang
#define KNAP             // Digital indgang

/*
 * Nedenståenden konflikter med SPI bussen (og hinanden)
 */
#define LYD1            10 // PWM udgang
#define LYD1            11 // PWM udgang
#define SERVO1          10 // PWM udgang
#define SERVO2          11 // PWM udgang

#define STOP 0x00
#define MEGET_LANGSOM   0x40
#define LANGSOM         0x70
#define ROLIG           0x90
#define RASK            0xB5
#define HURTIG          0xD0
#define RIGTIG_HURTIG   0xFF

typedef enum {
  rSTOP,
  rFREM,
  rTILBAGE,
  rVENSTRE,
  rHOEJRE,
} retning_t;

typedef enum {
  mVENSTRE,
  mHOEJRE,
  mBEGGE
} motorId_t;

class Motor {
  public:
    const uint8_t fremadPin, tilbagePin, aktivPin;
    uint8_t fremad, tilbage, aktiv;

    Motor(uint8_t fremadPin, uint8_t tilbagePin, uint8_t aktivPin) 
        : fremadPin(fremadPin), tilbagePin(tilbagePin), aktivPin(aktivPin), 
        fremad(0), tilbage(0), aktiv(0) {
      pinMode(fremadPin, OUTPUT);
      pinMode(tilbagePin, OUTPUT);
      pinMode(aktivPin, OUTPUT);
    }

    void koer(retning_t retning, uint8_t hastighed) {
      digitalWrite(fremadPin, retning == rFREM ? HIGH : LOW);
      digitalWrite(tilbagePin, retning == rTILBAGE ? HIGH : LOW);
      analogWrite(aktivPin, hastighed);
    }
};

Motor motor1(MOTOR1_FREM_AD, MOTOR1_TILBAGE, MOTOR1_AKTIV);
Motor motor2(MOTOR2_FREM_AD, MOTOR2_TILBAGE, MOTOR2_AKTIV);


void setup() {
}

void loop() {
  motor1.koer(rFREM, ROLIG);
  motor2.koer(rFREM, ROLIG);
  delay(1000);
  motor1.koer(rFREM, RIGTIG_HURTIG);
  motor2.koer(rFREM, ROLIG);
  delay(1000);
  motor1.koer(rFREM, ROLIG);
  motor2.koer(rFREM, RIGTIG_HURTIG);
  delay(1000);
  motor1.koer(rSTOP, ROLIG);
  motor2.koer(rSTOP, ROLIG);
  delay(1000);
  motor1.koer(rTILBAGE, ROLIG);
  motor2.koer(rTILBAGE, ROLIG);
  delay(2000);
}
